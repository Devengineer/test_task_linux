#include "myarea.h"
#include <cairomm/context.h>
#include <iostream>

MyArea::MyArea()
{
	// size of area
	set_size_request (200, 200);
}

MyArea::~MyArea()
{
}

bool MyArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();

  cr->set_line_width(10.0);

  // draw red lines out from the center of the window
  cr->set_source_rgb(0.8, 0.0, 0.0);
  cr->move_to(10, height);
  cr->line_to(10, height - 50);
  cr->move_to(30, height);
  cr->line_to(30, height - 30);
  cr->move_to(50, height);
  cr->line_to(50, height - 80);
  cr->move_to(70, height);
  cr->line_to(70, height - 140);
  cr->move_to(90, height);
  cr->line_to(90, height - 70);
  cr->move_to(110, height);
  cr->line_to(110, height - 10);
  cr->move_to(130, height);
  cr->line_to(130, height - 50);
  cr->move_to(150, height);
  cr->line_to(150, height - 90);
  cr->move_to(170, height);
  cr->line_to(170, height - 70);
  cr->move_to(190, height);
  cr->line_to(190, height - 150);
  
  // draw
  cr->stroke();
  /*
  cr->move_to(0, 0);
  cr->line_to(xc, yc);
  cr->line_to(0, height);
  cr->move_to(xc, yc);
  cr->line_to(width, yc);
  cr->stroke();
  */

  return true;
}
