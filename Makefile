CC=g++
CFLAGS=-c -Wall
CLIENT_FLAGS=`pkg-config --cflags --libs gtkmm-3.0`
SERVER_FLAGS=-lpthread

all: client server

client: client.o myarea.o window.o
	$(CC) client.o myarea.o window.o -o client $(CLIENT_FLAGS)
  
client.o: client.cc
	$(CC) $(CFLAGS) client.cc $(CLIENT_FLAGS)

myarea.o: myarea.cc
	$(CC) $(CFLAGS) myarea.cc $(CLIENT_FLAGS)

window.o: window.cc
	$(CC) $(CFLAGS) window.cc $(CLIENT_FLAGS)
	
server: server.o
	$(CC) server.o -o server $(SERVER_FLAGS)

clean:
	rm -rf *.o
