#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <netinet/in.h>
#include <resolv.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <gtkmm.h>
#include <iostream>
#include "window.h"
#include "myarea.h"

// Connect to server
void Connect() {
  int host_port= 1101;
  const char* host_name="127.0.0.1";

  struct sockaddr_in my_addr;

  char buffer[1024];
  int bytecount;
  int buffer_len=0;

  int hsock;
  int * p_int;
  int err;

  hsock = socket(AF_INET, SOCK_STREAM, 0);
  if(hsock == -1) {
    printf("Error initializing socket %d\n",errno);
    goto FINISH;
  }
    
  p_int = (int*)malloc(sizeof(int));
  *p_int = 1;
        
  if( (setsockopt(hsock, SOL_SOCKET, SO_REUSEADDR, (char*)p_int, sizeof(int)) == -1 )||
    (setsockopt(hsock, SOL_SOCKET, SO_KEEPALIVE, (char*)p_int, sizeof(int)) == -1 ) ){
    printf("Error setting options %d\n",errno);
    free(p_int);
    goto FINISH;
  }
  free(p_int);

  my_addr.sin_family = AF_INET ;
  my_addr.sin_port = htons(host_port);
    
  memset(&(my_addr.sin_zero), 0, 8);
  my_addr.sin_addr.s_addr = inet_addr(host_name);

  if( connect( hsock, (struct sockaddr*)&my_addr, sizeof(my_addr)) == -1 ){
    if((err = errno) != EINPROGRESS) {
      fprintf(stderr, "Error connecting socket %d\n", errno);
      goto FINISH;
    }
  }

  //Now lets do the client related stuff

  buffer_len = 1024;

  memset(buffer, '\0', buffer_len);

  printf("Enter some text to send to the server (press enter)\n");
  fgets(buffer, 1024, stdin);
  buffer[strlen(buffer)-1]='\0';
    
  if( (bytecount=send(hsock, buffer, strlen(buffer),0))== -1){
    fprintf(stderr, "Error sending data %d\n", errno);
    goto FINISH;
  }
  printf("Sent bytes %d\n", bytecount);

  if((bytecount = recv(hsock, buffer, buffer_len, 0))== -1){
    fprintf(stderr, "Error receiving data %d\n", errno);
    goto FINISH;
  }
  printf("Recieved bytes %d\nReceived string \"%s\"\n", bytecount, buffer);

  close(hsock);
  FINISH:
  ;
}

// Connect signal
void on_button1_clicked()
{
  std::cout << "Start connection" << std::endl;
  return Connect();
}

// Show graph signal
void on_button2_clicked()
{
  std::cout << "Show graph" << std::endl;
  Gtk::MessageDialog win("Graph", false, Gtk::MESSAGE_OTHER, Gtk::BUTTONS_CLOSE, true);
	Gtk::Box* box = win.get_content_area();
  MyArea area;
  box->add(area);
  area.show();
  win.run();
}

int main(int argc, char** argv){
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");
  Gtk::Window window;
  window.set_default_size(400, 200);
  
  
  // Child widgets:
  Gtk::Box m_box1;
  Gtk::Button m_button1("Connect to server"), m_button2("Show graph");
  window.set_title("Client app");
  // sets the border width of the window.
  window.set_border_width(10);

  // put the box into the main window.
  window.add(m_box1);

  // Now when the button is clicked, we call the "on_button_clicked" function
  // with a pointer to "button 1" as it's argument
  m_button1.signal_clicked().connect(sigc::ptr_fun(&on_button1_clicked));
  m_button2.signal_clicked().connect(sigc::ptr_fun(&on_button2_clicked));

  // instead of gtk_container_add, we pack this button into the invisible
  // box, which has been packed into the window.
  // note that the pack_start default arguments are Gtk::EXPAND | Gtk::FILL, 0
  m_box1.pack_start(m_button1);
  m_box1.pack_start(m_button2);


  // always remember this step, this tells GTK that our preparation
  // for this button is complete, and it can be displayed now.
  m_button1.show();
  m_button2.show();
  m_box1.show();


  //Shows the window and returns when it is closed.
  return app->run(window);
}
